Bonjour,
j'ai choisi de présenter les différentes étapes à suivre pour réaliser l'installation d'un "double boot".
  
  - Tout d'abord assurez-vous de bien avoir au moins 50 GB d'espace stockage vide.
  - mettez sur une clé USB différente votre clé de restauration du système:
        - aller dans vos paramètres 
        - taper : clé de restauration.
        - copier sur une clé. (cela peut prendre plusieurs minutes)

Le système est prêt pour installer un "double boot".

1. Avoir une clé usb vide:

 Pour réaliser cette installation vous devez commencer par vous munir d'une clé usb vide.


2. Installation de xUbuntu et de rufus: 

Pour la suite de l'installation vous allez devoir installer deux programmes:
    - l'ISO xUbuntu
    - Rufus (Permet de créer une clé bootable.)

-Lien de téléchargement de l'ISO xUbuntu: https://xubuntu.fr/
-Lien de téléchargement de Rufus: https://rufus.ie/fr/

3. Bouter sa clé via rufus: 

Ci-dessous la procédure pour booter sur une clé usb:
    - Ouvrir Rufus
    - Choisir sa clé dans les périphériques
    - Dans le type de démarage vous allez choisir votre iso (xUbuntu) donc vous allez cliquez sur SELECTION et y mettre votre xUbuntu
    - Pour finir vous aller appuyez sur demarer (prenez bien une clé vide car cela va effacer les donnés mise sur la clé).

Vous avez maintenant une clé usb bootable.

4. Redémarrage: 

Pour redémarrer sur une clé USB.
    - allez dans vos paramètres 
    - mise à jour et sécurité
    - récupération 
    - redémarrer maintenant

5. Le BIOS (étape si nécessaire dans le cas où l'étape 4 n'est pas suffisante):

Dans cette étape nous allons naviguer dans le BIOS de notre ordinateur.
au moment du redémarrage appuyer plusieurs fois sur la touche F12 ( Attention la touche peut différer suivant le modéle du PC utiliser)
aller dans l'onglet redélarrage
modifier l'ordre de redémarrage pour positioner cle usb en premier
sauvegarder et redémmarrer



6. Redémarrer sur la clé:

Vous allez avoir plusieur choix (continuer, utiliser un périphérique, éteindre):
    - vous choisirer utiliser un périphérique
    - USB drive (UEFI)

7. Les questions:

Pour cette dérniere étapes vous aller devoir répondre a quelques questions.

- ! ATTENTION vous ne devrais absolument pas vous connecter a un resaux internet (sinon ça ne fonctionne pas).
- installer les logiciels requis



Felicitation, vous avez votre Dual boot.

si vous voulez repartir sur windows il vous suffit de redemarer votre ordinateur normalement et choisir windows.
si ensuite vous voulez passer sur xUbuntu il vous suffit de redemarrer et de choisir xUbuntu.

voici mon lien gitlab si besoin: https://gitlab.com/ahmet.baba/sae_install

